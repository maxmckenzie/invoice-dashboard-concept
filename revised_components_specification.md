# Revised Components Specification

Given the "research" done on possible customisations the user base may want we can now add some more details to our Components Specific.

- invoice_list
  - invoice_list_item
    - invoice_list_item_client
    - invoice_list_item_description
    - invoice_list_item_issue-date
    - invoice_list_item_due-date
    - invoice_list_item_total-amount
    - invoice_list_item_currency `new`
    - invoice_list_item_tax `new`
    - invoice_list_item_net `new`
    - invoice_list_item_edit
    - invoice_list_item_send
    - invoice_list_item_delete
  - invoice_list_pagination
- invoice_filter
  - invoice_filter_date
  - invoice_filter_payment-status
  - invoice_filter_search
- balance_graph
  - balance_graph_view
  - balance_graph_duration-filter
- clients_overview
  - clients_overview_item
    - clients_overview_item_total
    - clients_overview_item_avarage-settle
    - clients_overview_item_percentage-of-total
- tax_overview  `new`
  - tax_overview-payments-due
  - tax_overview-payments-due--authority

> within the interface i've included the two new feature suggestions from the user stories `clients_overview` and `balance_graph`

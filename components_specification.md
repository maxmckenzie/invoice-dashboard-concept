# Components Specification

The Components Specification outlines the core elements of the interface. In regards to the naming and structure of the specification. It can be used to define the specific class structure and the states of the page. The advantage with this is you will need to involve the developers to define it. Having them involved early in the specification and planning. In this example BEM is used to write a Pseudocode guide for the styles and modifier structures.

> The user stories now serve as our guide to which elements will be needed.

- invoice_list
  - invoice_list_item
    - invoice_list_item_client
    - invoice_list_item_Description
    - invoice_list_item_issue-date
    - invoice_list_item_due-date
    - invoice_list_item_total-amount
    - invoice_list_item_edit
    - invoice_list_item_send
    - invoice_list_item_delete
- invoice_filter
  - invoice_filter_date
  - invoice_filter_payment-status
  - invoice_filter_search
- balance_graph
  - balance_graph_view
  - balance_graph_duration-filter
- clients_overview
  - clients_overview_item
    - clients_overview_item_total
    - clients_overview_item_avarage-settle
    - clients_overview_item_percentage-of-total

> within the interface i've included the two new feature suggestions from the user stories `clients_overview` and `balance_graph`

# Testing and User feedback

As with any part of a website we want to be able to track metrics and gather valuable information. We also want to ensure that the interface functions and does not fail the user by having regression and intergration issues arise.

To test the interface we can take the exsisting user stories we have and create cucumber test steps with a library such as [webdriver.io](https://github.com/webdriverio/cucumber-boilerplate)

We can also implement google analytics and upon design iterations A/B test the interface.

# Components Mock Up

Once we have a revised components specification we can start to design so hi-fidelity mock ups of the components. Fitting in and achieving the desired outcomes from the user stories in as simple a way as possible.

The following are images of affinity designer mockup using the Grade UI kit, and the basic HTML DOM structure.

> Obviously its cheating a bit to use a UI kit. But i'm just taking basic typography, styles and common patterns from it in lieu of a style guide, or the time to make a style guide.

## invoice_filter
![invoice_filter](http://i.imgur.com/oWr4O8v.png)

Template:
```HTML
<div class="invoice_filter">
  <span class="invoice_filter_date"><select name="" id=""></select></span>
  <span class="invoice_filter_payment-status"><select name="" id=""></select></span>
  <span class="invoice_filter_search"><input type="text" Search=""></span>
</div>
```

## invoice_list_item
![invoice_list_item](http://i.imgur.com/Tf0WFQ2.png)

Template:
```HTML
<div class="invoice_list_item">
  <h1 class="invoice_list_item_client">Client: Stuff and Things</h1>
  <p class="nvoice_list_item_description">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
  <div class="button-group">
    <button class="invoice_list_item_edit">edit</button>
    <button class="invoice_list_item_send">Send</button>
    <button class="invoice_list_item_delete">delete</button>
  </div>
  <div class="invoice_list_item_details">
    <div class="invoice_list_item_issue-date">
      <h2>Issue Date</h2>
      <p>1/12/2015</p>
    </div>
    <div class="invoice_list_item_due-date">
      <h2>Due Date</h2>
      <p>7/12/2015</p>
    </div>
    <div class="invoice_list_item_total-amount">
      <h2>Total</h2>
      <p>£3000</p>
    </div>
    <div class="invoice_list_item_currency">
      <h2>Currancy</h2>
      <p>€ Euros</p>
    </div>
    <div class="invoice_list_item_tax">
      <h2>Tax</h2>
      <p>£1000</p>
    </div>
    <div class="invoice_list_item_net">
      <h2>Invoice Net</h2>
      <p>7 Days</p>
    </div>
  </div>
</div>
```

## invoice_list_pagination
![invoice_list_pagination](http://i.imgur.com/kIDJsoP.png)

```HTML
<div class="invoice_list_pagination">
  <span class="invoice_list_pagination_newer">newer</span>
  <span>
    <span class="invoice_list_pagination--1">-1</span>
    <span class="invoice_list_pagination--2">-2</span>
    <span class="invoice_list_pagination--3">-3</span>
    <span class="invoice_list_pagination--4">-4</span>
    <span class="invoice_list_pagination--5">-5</span>
    <span class="invoice_list_pagination--6">-6</span>
    <span class="invoice_list_pagination--7">-7</span>
    <span class="invoice_list_pagination--8">-8</span>
  </span>
    <span class="invoice_list_pagination_older">older</span>
</div>
```

## tax_overview
![tax_overview](http://i.imgur.com/QJwj67d.png)

```HTML
<div class="tax_overview">
  <h1 class="tax_overview_title">tax Overview</h1>
  <div class="tax_overview_list">
    <div class="tax_overview_item">
      <h1>VAT <small>Due 22/12/2016</small></h1>
      <span class="tax_overview-payments-due--authority">
        <span>Spain: €0</span>
        <span>France: €0</span>
        <span>UK: €0</span>
      </span>
      <span class="tax_overview-payments-due">
        <span>Amount Due</span>
        <span>Due Date</span>
      </span>
    </div>
    <div class="tax_overview_item">...</div>
    <div class="tax_overview_item">...</div>
    <div class="tax_overview_item">...</div>
  </div>
</div>
```

## clients_overview
![clients_overview](http://i.imgur.com/81v1eCd.png)

```HTML
<div class="clients_overview">
  <h1 class="clients_overview_title">tax Overview</h1>
  <div class="clients_overview_list">
    <div class="clients_overview_item">
      <h1>Client Name</h1>
      <span class="clients_overview_item_avarage-settle">
        Client usually pays 5 days late
      </span>
      <span class="clients_overview_item_percentage-of-total">
        <span>Spent to date: £0000</span>
        <span>Client Value: 10% of sales</span>
      </span>
    </div>
    <div class="clients_overview_item">...</div>
    <div class="clients_overview_item">...</div>
    <div class="clients_overview_item">...</div>
  </div>
</div>
```

## balance_graph
![balance_graph](http://i.imgur.com/fsoJ44u.png)

```HTML
// This one would likely be a JS dynamic charting Library using D3.js
// the HTML for which is dependant on configuration
```

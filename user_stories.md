# User Stories

The first set of user stories is based on the brief

```gherkin
As a user, i want to

  See all invoices

  In each line of invoice data, i want to see the following information
  | Client | Description | Issue date | due date | total amount | "edit" | "send" | "delete” |

  As a user, i want to filter the list of results by
  | date | payment status |

  As a user, i want to search all invoices
```

The second set of user stories are suggestions additional to the brief. The idea being that the interface helps to guide more business based decisions.

```
As a user, i want to

  See the following details of each client
  | Total Monies paid (all time) | Average invoice settle time | Percentage of Total Business |

  See how many days over due the invoice is

  see a graph of my balance over the last year

```

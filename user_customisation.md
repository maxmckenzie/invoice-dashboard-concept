# User Customisation

With SAAS based projects it becomes necessary to adapt the features in line with feedback from your user base.

Knowing what components and user stories we have so far we can detail any specific user based custom scenarios for the interface. Such as permissions, currencies, and roles.

> This information would be gathered in the field by conducting interviews and drawing parallels with each user interview. The below is obviously off the top of my head for the purpose of demonstration

```
as a user, i want to

  see how much I owe in tax

  see when the tax i owe is due to be paid out

  mark a taxable amount as paid
    and enter a receipt reference

  have a multiple currencies for each of my clients
    // if there is more than one currency active in the
    // users account the following stories apply

    have the currencies depicted as separate lines in my balance graph

    have XE currency rates displayed as a widget

    see a breakdown of tax owed to different countries
```

The above drastically changes the nature of our application structure. Had we gone to a developer with just the user stories waited three months for him to finish. Adding these after the fact would make him pretty upset. There was no mention of currency conversion tax calculation.

(┛◉Д◉)┛彡┻━┻
> no developer wants to be surprised by multi currency and tax calculations

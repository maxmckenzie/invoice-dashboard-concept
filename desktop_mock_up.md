# Desktop Mock up

With all the components mocked up they can be assembled to create the layout as the desktop example below shows

![desktop mock up](http://i.imgur.com/edQli30.png)

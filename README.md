# Brief
Design a one page web app that contains a list of invoices.
Each "line of invoice data” contains:
- Client, Concept, Issue date, due date, total amount and an "action" button which contains "edit" , "send" and a "destroy” button.

You should also add some filters
- Datepicker
- Search
- Payment Status

It must be a way to see "more information" about some of the elements without leaving the page nor refreshing.

> Git based activity can be found here https://gitlab.com/maxmckenzie/invoice-dashboard-concept/activity
> The final spec is hosted on gitbook https://maxmckenzie.gitbooks.io/invoice-dashoboard-concept/content/

# WorkFlow and contents
this project is based on the following work flow defining user stories first then the components specification and so on. As each section is complete within a team context feedback would be gathered from the stakeholders.

- User Stories
- Components Specification
- User Customisation
- Hand Draw Sketches
- Components Mock Up (Affinity Designer plus UI Pack)
- Testing and User feedback suggestions
- If time allows make the UI
